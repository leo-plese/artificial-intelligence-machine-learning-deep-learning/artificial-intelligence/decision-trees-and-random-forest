from sys import argv
import math


class Config():
    def __init__(self, mode, model, max_depth, num_trees, feature_ratio, example_ratio):
        self.mode = mode
        self.model = model
        self.max_depth = max_depth
        self.num_trees = num_trees
        self.feature_ratio = feature_ratio
        self.example_ratio = example_ratio




class Node():
    def __init__(self, feature, children=None):
        self.feature = feature
        self.children = children


class RF():
    def __init__(self, config):
        self.config = config

        self.createID3TreeObjects()

    def createID3TreeObjects(self):
        self.id3TreeObjs = []
        for i in range(self.config.num_trees):
            self.id3TreeObjs.append(ID3(self.config))


    def fit(self, D, Dprime, X, y):
        import random
        datasetSize = len(D)
        numFeatures = len(X)
        pickExamplesNum = round(self.config.example_ratio * datasetSize)
        pickFeaturesNum = round(self.config.feature_ratio * numFeatures)

        featureNameNumDictINIT, featureNumNameDictINIT = {}, {}
        for i in range(len(X)):
            featureNameNumDictINIT[X[i]] = i
            featureNumNameDictINIT[i] = X[i]

        featureNameNumDictionaries = []
        featureNums = []
        trainExamples = []

        self.trees = []
        for tr in self.id3TreeObjs:
            a = random.sample(list(enumerate(D)), pickExamplesNum)
            aInds = [x[0] for x in a]
            a = [x[1] for x in a]
            bName = random.sample(X, pickFeaturesNum)

            b = [featureNameNumDictINIT[f] for f in bName]
            featureNums.append(b)

            if len(set([x[-1] for x in a])) > 1:
                print(" ".join(bName))
            print(" ".join(list(map(str, aInds))))



            featureNameNumDict, featureNumNameDict = {}, {}
            for i in range(len(bName)):
                fname = bName[i]
                featureNameNumDict[fname] = i
                featureNumNameDict[i] = bName[i]

            #b = [featureNameNumDict[f] for f in bName]


            chosenExamplesFeaturesList = []
            for el in a:
                appList = [el[fnum] for fnum in b]+[el[-1]]
                chosenExamplesFeaturesList.append(appList)

            trainExamples.append(chosenExamplesFeaturesList)

            yDict = getClassLabelsDict([x[-1] for x in chosenExamplesFeaturesList])

            tree = tr.id3(chosenExamplesFeaturesList[:], chosenExamplesFeaturesList[:], bName, yDict, featureNameNumDict, featureNumNameDict, 0)
            self.trees.append(tree)

            featureNameNumDictionaries.append(featureNameNumDict)

        return featureNums, featureNameNumDictionaries, trainExamples

    def predict(self, featureNums, trainExamples, testDataList, featureNameNumDictionaries):
        allDecisions = []
        for i in range(self.config.num_trees):
            currFeatureNameNumDict = featureNameNumDictionaries[i]
            currTrainExamples = trainExamples[i]
            currFeatureNums = featureNums[i]
            currTree = self.trees[i]
            testDataListNew = []
            for x in testDataList:
                newEl = [x[el] for el in currFeatureNums]
                testDataListNew.append(newEl+[x[-1]])

            currID3Obj = self.id3TreeObjs[i]
            decisions = currID3Obj.predictRF(currTree, currTrainExamples, testDataListNew, currFeatureNameNumDict)
            allDecisions.append(decisions)


        testLen = len(testDataList)
        finalDecs = []
        for i in range(testLen):
            currDecs = []
            for j in range(self.config.num_trees):
                decJ = allDecisions[j]
                currDecs.append(decJ[i])

            #print("C =",currDecs)
            finalDecs.append(getMostFrequentClassLabel(getClassLabelsDict(currDecs)))
        #print("F",finalDecs)
        return finalDecs






class ID3():
    def __init__(self, config):
        self.mode = config.mode
        self.max_depth = config.max_depth

    def id3(self, D, Dprime, X, y, featureNameNumDict, featureNumNameDict, lev=0):

        if len(Dprime) == 0:
            v = getMostFrequentClassLabel(y)
            return Node(v)
        v  = getMostFrequentClassLabel(getClassLabelsDict([x[-1] for x in Dprime]))

        if len(X)==0 or sorted(Dprime)==sorted(getExamplesWithLabel(Dprime, v)) or lev == self.max_depth:
            return Node(v)

        x = getMostDiscriminativeFeature(Dprime, X, featureNameNumDict, featureNumNameDict) # index of feature name in feature dict featureNameNumDict

        subtrees = set()
        featureValueSet = getFeatureValueSet(Dprime, x)

        Xcpy = X[:]
        Xcpy.remove(featureNumNameDict[x])
        for v in featureValueSet:
            DprimeSubset = getExamplesWithFeatureValue(Dprime, x, v)

            t = self.id3(D, DprimeSubset, Xcpy, y, featureNameNumDict, featureNumNameDict, lev+1)
            subtrees.add((v, t))

        return Node(featureNumNameDict[x], subtrees)

    def fit(self, D, Dprime, X, y):
        #global featureNameNumDict, featureNumNameDict
        featureNameNumDict, featureNumNameDict = {}, {}
        for i in range(len(X)):
            featureNameNumDict[X[i]] = i
            featureNumNameDict[i] = X[i]
        tree = self.id3(D, Dprime, X, y, featureNameNumDict, featureNumNameDict, 0)

        self.tree = tree

        self.buildNodes = []
        self.printNode(tree)


        return featureNameNumDict

    def getBuildNodes(self, tree, level):
        if tree.children is None:
            return
        self.buildNodes.append((level, tree.feature))
        for c in tree.children:
            self.getBuildNodes(c[1], level + 1)

    def printNode(self, tree):
        self.getBuildNodes(tree, 0)
        self.buildNodes.sort(key=lambda x: x[0])
        for i in range(len(self.buildNodes)-1):
            print("{}:{}, ".format(*self.buildNodes[i]), end="")
        print("{}:{}".format(*self.buildNodes[-1]))

    def findChildForFeatureValue(self, fVal, treeChildren):
        for c in treeChildren:
            if c[0] == fVal:
                return c[1]



    def getLabelForTestExample(self, trainExamples, example, tree, featureNameNumDict):
        f = tree.feature
        children = tree.children

        if children is None:
            return f

        fn = featureNameNumDict[f]
        fVal = example[fn]

        trainExamplesSubset = trainExamples

        child = self.findChildForFeatureValue(fVal, children)
        if child is not None:
            trainExamplesSubset = [x for x in trainExamples if x[fn] == fVal]
        else:
            nodeChildrenLabels = [x[-1] for x in trainExamplesSubset]
            return getMostFrequentClassLabel(getClassLabelsDict(nodeChildrenLabels))  # mostFrequentClassLabel

        return self.getLabelForTestExample(trainExamplesSubset, example, child, featureNameNumDict)


    def predict(self, trainExamples, testExamples, featureNameNumDict):
        tree = self.tree
        f = tree.feature
        fn = featureNameNumDict[f]
        decisions = []
        i=0
        for ex in testExamples:
            decision = self.getLabelForTestExample(trainExamples, ex, tree, featureNameNumDict)
            decisions.append(decision)
            i+=1

        return decisions


    def predictRF(self, tree, trainExamples, testExamples, featureNameNumDict):
        decisions = []
        i=0
        for ex in testExamples:
            decision = self.getLabelForTestExample(trainExamples, ex, tree, featureNameNumDict)
            decisions.append(decision)
            i+=1

        return decisions



def getSetEntropy(examples):
    classLabelsDict = getClassLabelsDict([x[-1] for x in examples])
    totExamples = len(examples)

    entropy = 0
    for lab in classLabelsDict:
        curLabOutOfAllRatio = classLabelsDict[lab] / totExamples
        entropy -= curLabOutOfAllRatio*math.log2(curLabOutOfAllRatio)

    return entropy

def getFeatureValueSet(examples, featureNum):
    return set([ex[featureNum] for ex in examples])

def getExamplesWithFeatureValue(examples, featureNum, fVal):
    return [ex for ex in examples if ex[featureNum]==fVal]


def calcIG(examples, featureNum):
    E0 = getSetEntropy(examples)

    expectedEntropyAfterDiv = 0
    featureValueSet = getFeatureValueSet(examples, featureNum)
    for fVal in featureValueSet:
        subset = getExamplesWithFeatureValue(examples, featureNum, fVal)
        expectedEntropyAfterDiv += len(subset)/len(examples) * getSetEntropy(subset)

    IG = E0 - expectedEntropyAfterDiv

    return IG


def getMostDiscriminativeFeature(examples, X, featureNameNumDict, featureNumNameDict):
    featureIGDict = {}
    for x in X:
        fn = featureNameNumDict[x]
        featureIGDict[fn] = calcIG(examples, fn)

    if mode != "test":
        featureIGDictCpy = featureIGDict.copy()
        while len(featureIGDictCpy) > 0:
            maxVal = max(featureIGDictCpy.values())
            maxDictKeys = [k for k in featureIGDictCpy if featureIGDictCpy[k]==maxVal]
            for di in maxDictKeys:
                featureIGDictCpy.pop(di)
            maxDictKeys = sorted(maxDictKeys, key=lambda k: featureNumNameDict[k])
            for k in maxDictKeys:
                print("IG(%s)=%.4f  " % (featureNumNameDict[k], featureIGDict[k]), end="")  # i ovo abecednim redom
        print()



    maxIG = max(featureIGDict.values())
    a = [k for k in featureIGDict if featureIGDict[k] == maxIG]  # can be more than one attribute with same max IG
    bDict = {}
    for k in a:
        bDict[featureNumNameDict[k]] = k

    fNumsSorted = sorted(bDict.keys())  # alphabetical order

    mostDiscrFeature = bDict[fNumsSorted[0]]
    return mostDiscrFeature



def getExamplesWithLabel(examples, v):
    return [x for x in examples if x[-1]==v]

def getMostFrequentClassLabel(labelsDict):
    maxValue = max(labelsDict.values())
    a = [k for k in labelsDict if labelsDict[k] == maxValue] # can be more than one label with same max number of examples
    a.sort() #alphabetical order
    return a[0]


def getFeatureNames(line):
    fs = line.split(",")
    return fs[:-1], fs[-1]


def getLines(datasetDescr):
    lines = datasetDescr.readlines()
    return [line.strip() for line in lines if len(line.strip()) > 0]

def getDatasetExamples(lines):
    return [tuple(line.split(",")) for line in lines]

def getClassLabels(lines):
    return [line.split(",")[-1].strip() for line in lines]

def getClassLabelsDict(clsLabs):
    clsLabDict = {}
    for lab in clsLabs:
        if lab not in clsLabDict:
            clsLabDict[lab] = 1
        else:
            clsLabDict[lab] += 1

    return clsLabDict


def getHyperparamVal(line):
    return line.split("=")[1].strip()

def loadConfig(lines):
    # mode = "test"
    # model = "ID3"
    global mode
    max_depth = math.inf
    num_trees = 1
    feature_ratio = 1
    example_ratio = 1

    for line in lines:

        fNameVal = [x.strip() for x in line.split("=")]

        hpName = fNameVal[0]
        hpVal = fNameVal[1]

        if hpName == "mode":
            mode = hpVal
        elif hpName == "model":
            model = hpVal
        elif hpName == "max_depth":
            if hpVal == "-1":
                continue
            max_depth = int(hpVal)
        elif hpName == "num_trees":
            num_trees = int(hpVal)
        elif hpName == "feature_ratio":
            feature_ratio = float(hpVal)
        elif hpName == "example_ratio":
            example_ratio = float(hpVal)

    return Config(mode, model, max_depth, num_trees, feature_ratio, example_ratio)



def loadData(trainSetPth, testSetPth, configPth):

    with open(trainSetPth,"r",encoding="utf-8") as trainSetDescr:
        lines = getLines(trainSetDescr)
        featureNames, targetFeatureName = getFeatureNames(lines[0])
        dsExampleLines = lines[1:]
        trainDataList = getDatasetExamples(dsExampleLines)

        classLabelsList = getClassLabels(dsExampleLines)
        classLabels = getClassLabelsDict(classLabelsList)

    with open(testSetPth, "r", encoding="utf-8") as testSetDescr:
        lines = getLines(testSetDescr)
        testDataList = getDatasetExamples(lines[1:])

    with open(configPth,"r",encoding="utf-8") as configDescr:
        lines = getLines(configDescr)
        config = loadConfig(lines)

    return classLabelsList, classLabels, featureNames, targetFeatureName, trainDataList, testDataList, config


def getAccuracy(testGroundTruth, decisions):
    total = len(decisions)
    correct = sum([1 if decisions[i]==testGroundTruth[i] else 0 for i in range(total)])

    return correct/total

def getConfusionMatrix(clsLabsSorted, testGroundTruth, decisions):
    clsLabDict = {}
    i=0
    for lab in clsLabsSorted:
        clsLabDict[lab] = i
        i += 1

    totClassLabels = len(clsLabsSorted)
    setLen = len(testGroundTruth)
    confMat = [[0]*totClassLabels for _ in range(totClassLabels)]

    for i in range(totClassLabels):
        labelX = clsLabsSorted[i]
        labGTInds = [j for j in range(setLen) if testGroundTruth[j]==labelX]
        decisionsAtLabGTInds = [decisions[k] for k in labGTInds]

        row = confMat[i]
        for j in range(totClassLabels):
            labelY = clsLabsSorted[j]
            labelYnum = clsLabDict[labelY]

            row[labelYnum] = sum([1 if dec==labelY else 0 for dec in decisionsAtLabGTInds])

    return confMat





if __name__=="__main__":
    trainSetPth = argv[1]
    testSetPth = argv[2]
    configPth = argv[3]

    classLabelsList, classLabels, featureNames, targetFeatureName, trainDataList, testDataList, config = loadData(trainSetPth, testSetPth, configPth)


    # print("--- CONFIG ---")
    # print("mode <" + config.mode + ">")
    # print("model <" + config.model + ">")
    # print("max_depth <" + str(config.max_depth) + ">")
    # print("num_trees <" + str(config.num_trees) + ">")
    # print("feature_ratio <" + str(config.feature_ratio) + ">")
    # print("example_ratio <" + str(config.example_ratio) + ">")
    # print()
    #
    # print("FEATURES <" + str(featureNames) + "> <" + str(targetFeatureName) + ">")
    # print("LABELS y =", classLabels, getMostFrequentClassLabel(classLabels))
    mostFrequentClassLabel = getMostFrequentClassLabel(classLabels)

    # print("---TRAIN---")
    # i=0
    # for x in trainDataList:
    #     print(i,x)
    #     i+=1
    # print()
    #
    # print("---TEST---")
    # i=0
    # for x in testDataList:
    #     print(i,x)
    #     i += 1
    # print()

    if config.model == "ID3":
        #print("********")
        id3Model = ID3(config)
        featureNameNumDict = id3Model.fit(trainDataList[:], trainDataList[:], featureNames, classLabels) # set D, set D', list X, dict y
        decisions = id3Model.predict(trainDataList, testDataList, featureNameNumDict)



    else:
        rfModel = RF(config)
        featureNums, featureNameNumDictionaries, trainExamples = rfModel.fit(trainDataList[:], trainDataList[:], featureNames, classLabels) # set D, set D', list X, dict y
        decisions = rfModel.predict(featureNums, trainExamples, testDataList, featureNameNumDictionaries)

    for d in range(len(decisions) - 1):
        print(decisions[d], end=" ")
    print(decisions[-1])
    testGroundTruth = [x[-1] for x in testDataList]
    # print("C",testGroundTruth)
    # print("D",decisions)
    clsLabsSorted = sorted(classLabels.keys())
    # print(clsLabsSorted)
    accuracy = getAccuracy(testGroundTruth, decisions)
    print("%.5f" % accuracy)
    confMat = getConfusionMatrix(clsLabsSorted, testGroundTruth, decisions)
    #print(confMat)
    for row in confMat:
        print(" ".join(list(map(str, row))))
